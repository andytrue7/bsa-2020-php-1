<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    private string $title;
    private int $price;
    private int $pagesNumber;

    public function __construct($title, $price, $pagesNumber)
    {
        $this->title = $title;

        if ($price < 0) {
            throw new \InvalidArgumentException('negative price');
        }
        $this->price = $price;

        if ($pagesNumber < 0) {
            throw new \InvalidArgumentException('negative number of pages');
        }
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}