<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private int $minPagesNumber;
    private array $libraryBooks;
    private int $maxPrice;
    private array $storeBooks;

    public function __construct($minPagesNumber, $libraryBooks, $maxPrice, $storeBooks)
    {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public function generate(): \Generator
    {
        foreach ($this->libraryBooks as $libraryBook) {

            if ($libraryBook->getPagesNumber() < $this->minPagesNumber || $libraryBook == null) {
                continue;
            }
            yield $libraryBook;
        }

        foreach ($this->storeBooks as $storeBook) {

            if ($storeBook->getPrice() > $this->maxPrice || $storeBook == null) {
                continue;
            }
            yield $storeBook;
        }
    }
}