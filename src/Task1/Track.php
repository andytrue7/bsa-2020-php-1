<?php

declare(strict_types=1);

namespace App\Task1;



class Track
{
    private float $lapLength;
    private int $lapsNumber;
    private array $cars = [];
    private array $finishedCars = [];

    public function __construct(float $lapLength, int $lapsNumber)
    {
        if ($lapLength < 0) {
            throw new \InvalidArgumentException('negative lap length');
        }
        $this->lapLength = $lapLength;

        if ($lapsNumber < 0) {
            throw new \InvalidArgumentException('negative laps number');
        }
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        if (empty($this->cars)) {
            throw new \Exception('no cars added');
        }
        return $this->cars;
    }

    public function run(): Car
    {
        foreach ($this->all() as $car) {

            $this->finishedCars[$this->trackLogic($car)] = $car;

        }
        return $this->finishedCars[min(array_keys($this->finishedCars))];
    }

    private function trackLogic(Car $car): float
    {
        //total track distance in km.
        $totalDistance = $this->lapLength * $this->lapsNumber;

        // total time without pit stop in sec.
        $totalTimeWithoutPitStop = ($totalDistance / $car->getSpeed()) * 3600;

        // fuel duration in km.
        $fuelDuration = round(($car->getFuelTankVolume() / $car->getFuelConsumption()) * 100);

        // total pit stop time in sec.
        $totalPitStopTime = round($totalDistance / $fuelDuration,2) * $car->getPitStopTime();

        // total time with pit stop time in sec.
        $totalTimeWithPitStop = $totalTimeWithoutPitStop + $totalPitStopTime;

        return $totalTimeWithPitStop;
    }
}

