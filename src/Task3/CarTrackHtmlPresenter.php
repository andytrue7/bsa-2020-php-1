<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $carPresentation = '';

        foreach ($track->all() as $car) {
            $carPresentation .= "<div><p>{$car->getName()}: {$car->getSpeed()}, {$car->getPitStopTime()}</p> <img src=\"{$car->getImage()}\"></div>";
        }
        return $carPresentation;
    }
}